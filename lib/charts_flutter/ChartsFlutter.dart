import 'package:flutter/material.dart';
import 'package:flutter_chart_app/charts_flutter/HorizontalBarChart.dart';

class ChartsFlutter extends StatelessWidget {
  const ChartsFlutter({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Charts Flutter'),
      ),
      body: Container(
        child: Center(
          child: SizedBox(
            height: 200,
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Container(
                clipBehavior: Clip.hardEdge,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: HorizontalBarChart.withSampleData(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
