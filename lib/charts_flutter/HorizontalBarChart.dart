// Copyright 2018 the Charts project authors. Please see the AUTHORS file
// for details.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// Horizontal bar chart example
// EXCLUDE_FROM_GALLERY_DOCS_START
import 'dart:math';
// EXCLUDE_FROM_GALLERY_DOCS_END
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:charts_common/common.dart' as commonCharts;
import 'package:flutter_chart_app/charts_flutter/CustomCircleSymbolRenderer.dart';

class HorizontalBarChart extends StatelessWidget {
  static const secondaryMeasureAxisId = 'secondaryMeasureAxisId';

  final List<charts.Series> seriesList;
  final bool animate;

  HorizontalBarChart(this.seriesList, {this.animate});

  factory HorizontalBarChart.withSampleData() {
    return new HorizontalBarChart(
      _createSampleData(),
      animate: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    final customTickFormatter = charts.BasicNumericTickFormatterSpec(
        (num value) => '${value.toInt()} M');

    return new charts.BarChart(
      seriesList,
      animate: animate,
      animationDuration: Duration(milliseconds: 300),
      vertical: false,
      behaviors: [
        // charts.SelectNearest(
        //   eventTrigger: charts.SelectionTrigger.pressHold,
        // ),
        // charts.LinePointHighlighter(
        //   symbolRenderer: CustomCircleSymbolRenderer(),
        // )
      ],
      secondaryMeasureAxis: new charts.NumericAxisSpec(
        showAxisLine: false,
        tickFormatterSpec: customTickFormatter,
        tickProviderSpec: new charts.BasicNumericTickProviderSpec(
          desiredTickCount: 9,
        ),
        viewport: charts.NumericExtents(0, 80),
        renderSpec: new charts.GridlineRendererSpec(
          tickLengthPx: 10,
          labelOffsetFromAxisPx: 16,
          labelStyle: new charts.TextStyleSpec(
            fontSize: 12, // size in Pts.
            color: charts.Color.fromHex(code: "#666666"),
          ),
          lineStyle: new charts.LineStyleSpec(
            color: charts.Color.fromHex(code: "#dddddd"),
          ),
        ),
      ),
      domainAxis: new charts.OrdinalAxisSpec(
        showAxisLine: false,
        renderSpec: charts.NoneRenderSpec(),
        scaleSpec: commonCharts.FixedPixelOrdinalScaleSpec(20),
      ),
      layoutConfig: new charts.LayoutConfig(
        leftMarginSpec: charts.MarginSpec.fixedPixel(32),
        topMarginSpec: charts.MarginSpec.fixedPixel(32),
        rightMarginSpec: charts.MarginSpec.fixedPixel(32),
        bottomMarginSpec: charts.MarginSpec.fixedPixel(0),
      ),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData() {
    final salesData = [
      new OrdinalSales('saldo awal', 20),
      new OrdinalSales('penerimaan', 23),
      new OrdinalSales('pengeluaran', 30),
      new OrdinalSales('saldo akhir', 50),
    ];

    final colors = [
      charts.Color(r: 82, g: 159, b: 210),
      charts.Color(r: 247, g: 190, b: 119),
      charts.Color(r: 239, g: 121, b: 137),
      charts.Color(r: 106, g: 211, b: 106),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: salesData,
        colorFn: (_, index) => colors[index],
      )..setAttribute(charts.measureAxisIdKey, secondaryMeasureAxisId),
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
