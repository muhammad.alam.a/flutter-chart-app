import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('Charts Flutter'),
            onTap: () => Navigator.of(context).pushNamed('/charts_flutter'),
          ),
          ListTile(
            title: Text('MP Flutter Bar Horizontal'),
            onTap: () =>
                Navigator.of(context).pushNamed('/mp_flutter_bar_horizontal'),
          ),
          ListTile(
            title: Text('Flutter Echarts'),
            onTap: () => Navigator.of(context).pushNamed('/flutter_echarts'),
          ),
        ],
      ),
    );
  }
}
