import 'package:flutter/material.dart';
import 'package:flutter_chart_app/charts_flutter/ChartsFlutter.dart';
import 'package:flutter_chart_app/flutter_echarts/FlutterEcharts.dart';
import 'package:flutter_chart_app/home_page.dart';
import 'package:flutter_chart_app/mp_flutter/MPFlutterBarHorizontalView.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
      routes: <String, WidgetBuilder>{
        '/charts_flutter': (_) => ChartsFlutter(),
        '/mp_flutter_bar_horizontal': (_) => MPFlutterBarHorizontalView(),
        '/flutter_echarts': (_) => FlutterEcharts(),
      },
    );
  }
}
