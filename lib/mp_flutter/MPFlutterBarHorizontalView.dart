import 'package:flutter/material.dart';
import 'package:flutter_chart_app/mp_flutter/MPFlutterBarHorizontal.dart';

class MPFlutterBarHorizontalView extends StatelessWidget {
  const MPFlutterBarHorizontalView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MPFlutterBarHorizontal(),
    );
  }
}
